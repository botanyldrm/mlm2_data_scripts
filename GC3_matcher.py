from pathlib import Path
import openpyxl
import datetime
import json 

hour_delay = 6

def matcher_function(keys1, keys2):
    keys1_to_keys2 = dict()
    keys2_to_keys1 = dict()
    
    for k1 in keys1:
        for k2 in keys2:
            curr_diff = abs(float(k1) - float(k2))
            if curr_diff < error_margin:
                keys1_to_keys2["{}".format(k1)] = k2
                keys2_to_keys1["{}".format(k2)] = k1
                continue
            curr_diff = abs(curr_diff - hour_delay * 60 * 60) 
            if curr_diff < error_margin:
                keys1_to_keys2["{}".format(k1)] = k2
                keys2_to_keys1["{}".format(k2)] = k1
                continue
    return keys1_to_keys2, keys2_to_keys1

gc3_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/01_25_22/gt/GC3_Edited 1.25 MLM2.xlsx"
data_folder = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/01_25_22"
save_path = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/01_25_22/GC3_GT_01_25_22.json"


error_margin = 15
key_word = "Club"
names=None

gc3_dict = dict()
mlm2_dict = dict()
matched_dict = dict()

wb = openpyxl.load_workbook(gc3_pth)
ws = wb.active

for row in ws.iter_rows(values_only=True):
    if names is None:
        if key_word in row:
            names = row
    else:
        temp_dict = dict()
        for idx, n in enumerate(names):
            temp_dict[n] = row[idx]

        date = temp_dict["Date"]
        if isinstance(date, datetime.datetime):
            gc3_dict["{}".format(date.timestamp())] = temp_dict


source_path = Path(data_folder)
scn_folders = [str(s) for s in source_path.glob('**/RIFF_*/')]
RIFF_names=[s.split("/")[-1] for s in scn_folders]

for r in RIFF_names:
    date = datetime.datetime(int(r.split("_")[3]), \
                             int(r.split("_")[2]), \
                             int(r.split("_")[1]), \
                             int(r.split("_")[4]), \
                             int(r.split("_")[5]), \
                             int(r.split("_")[6]))
    mlm2_dict["{}".format(date.timestamp())] = r

gc3_to_mlm2, mlm2_to_gc3 = matcher_function(gc3_dict.keys(), mlm2_dict.keys())

count = 0
for m in mlm2_to_gc3:
    matched_dict[mlm2_dict[m]] = gc3_dict[mlm2_to_gc3[m]]
    matched_dict[mlm2_dict[m]]["Date"] = m
    matched_dict[mlm2_dict[m]]["MLM2Date"] = m
    matched_dict[mlm2_dict[m]]["GC3Date"] = mlm2_to_gc3[m]
    matched_dict[mlm2_dict[m]]["Count"] = count
    count+=1


j = json.dumps(matched_dict, indent=4)
f = open(save_path, "w")
# print >> f, j
f.write(j)
f.close()
    

