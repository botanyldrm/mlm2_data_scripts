import csv 
import sys
import openpyxl
import numpy as np
import matplotlib.pyplot as plt

gc3_pth_list = ["/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/4_02_2022/GC3 Clean Mike MLM2 2.4.xlsx"]
mlm2_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/4_02_2022/MLM2_Spin_Result.txt"
mlm2_gc3_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/4_02_2022/MLM2_GC3_Spin_Result.txt"
rpm_hist_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/4_02_2022/rpm_error.png"
sa_hist_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/4_02_2022/sa_error.png"

names = None
gc3_shot_list = []
mlm2_shot_list = []

match_gc3_2_mlm2 = dict()
match_mlm2_2_gc3 = dict()

for gc3_pth in gc3_pth_list:
    wb = openpyxl.load_workbook(gc3_pth)
    ws = wb.active

    counter = -1

    for row in ws.iter_rows(values_only=True):
        counter += 1
        if counter == 2:
            names = row
        elif counter == 0 or counter == 1:
            continue
        else:
            data_dict = dict()
            try:
                for i in range(len(names)):
                    if names[i] == "Date":
                        curr_date = row[i]
                        curr_h = int(curr_date.hour)
                        if curr_h >= 20:
                            curr_h -= 6
                        curr_m = int(curr_date.minute)
                        curr_s = int(curr_date.second)
                        data_dict["{}".format(names[i])] = "{}:{}:{}".format(curr_h, curr_m, curr_s)
                    else:
                        data_dict["{}".format(names[i])] = row[i]

            except:
                continue
            gc3_shot_list.append(data_dict)

with open(mlm2_pth, "r") as f:
    data = f.readlines()
    for d in data:
        curr_d = d.split(" ") 
        data_dict = dict()
        data_dict["spin_axis"] = float(d.split("SA: ")[-1])
        data_dict["spin_rate"] = d.split("RPM: ")[-1].split("SA: ")[0]
        data_dict["source_pth"] = d.split("Shot: ")[-1].split("RPM: ")[0]

        parts = data_dict["source_pth"].split("/")
        riff_name = [p for p in parts if "RIFF_" in p][0]
        riff_name = riff_name.split("_")
        date = "{}:{}:{}".format(riff_name[-3], riff_name[-2], riff_name[-1])
        data_dict["Date"] = date
        mlm2_shot_list.append(data_dict)

for i in range(len(gc3_shot_list)):
    best_error = 9999999
    insert_flag = False
    matched_idx = -1
    for j in range(len(mlm2_shot_list)):
        curr_gc3 = gc3_shot_list[i]
        curr_mlm2 = mlm2_shot_list[j]
        date_trackman = curr_gc3["Date"]
        date_mlm2 = curr_mlm2["Date"]

        h0 = date_trackman.split(":")[0]
        h1 = date_mlm2.split(":")[0]

        m0 = date_trackman.split(":")[1]
        m1 = date_mlm2.split(":")[1]

        s0 = date_trackman.split(":")[2]
        s1 = date_mlm2.split(":")[2]

        d0 = int(h0) * 3600 + int(m0) * 60 + int(s0)
        d1 = int(h1) * 3600 + int(m1) * 60 + int(s1)

        if abs(d0 - d1) < 25:
            if abs(d0 - d1) < best_error:
                best_error = abs(d0 - d1)
                matched_idx = j
                insert_flag = True
    if insert_flag:
        match_gc3_2_mlm2["{}".format(i)] = "{}".format(matched_idx)
        match_mlm2_2_gc3["{}".format(matched_idx)] = "{}".format(i)


f = open(mlm2_gc3_pth, "w")

rpm_error = []
sa_error = []
abs_rpm_error = []
abs_sa_error = []

for i in range(len(mlm2_shot_list)):
    curr_mlm = mlm2_shot_list[i]
    if "{}".format(i) in match_mlm2_2_gc3.keys():
        j = int(match_mlm2_2_gc3["{}".format(i)])
    else:
        j = None
    if not j is None:
        curr_gc3 = gc3_shot_list[j]

        if (curr_mlm["spin_rate"]=="No Trigger") or (curr_gc3["Spin Rate"]=="No Trigger"):
            continue
        if (curr_mlm["spin_rate"]=="No trigger") or (curr_gc3["Spin Rate"]=="No trigger"):
            continue
        if (curr_mlm["spin_rate"]=="-") or (curr_gc3["Spin Rate"]=="-"):
            continue
        
        if (not curr_mlm["spin_rate"] is None) and (not curr_gc3["Spin Rate"] is None) and\
            (curr_mlm["spin_rate"]!="No Trigger") and (curr_gc3["Spin Rate"]!="No Trigger"):
            
            rpm_error.append(float(curr_mlm["spin_rate"]) - float(curr_gc3["Spin Rate"]))
            abs_rpm_error.append(abs(float(curr_mlm["spin_rate"]) - float(curr_gc3["Spin Rate"])))
        else:
            continue
        if (not curr_mlm["spin_axis"] is None) and (not curr_gc3["Spin Axis Tilt"] is None) and\
            (curr_mlm["spin_axis"]!="No Trigger")and (curr_gc3["Spin Axis Tilt"]!="No Trigger"):
            
            sa_error.append(float(curr_mlm["spin_axis"]) - float( - curr_gc3["Spin Axis Tilt"]))
            abs_sa_error.append(abs(float(curr_mlm["spin_axis"]) - float( - curr_gc3["Spin Axis Tilt"])))
        else:
            continue
        try:
            try:
                f.write("Shot: {} MLM RPM: {:.2f} GC3 RPM: {:.2f} MLM SA: {:.2f} GC3 SA: {:.2f} RPM Error: {:.2f}\n".format(curr_mlm["source_pth"].split("USA/")[-1],\
                    float(curr_mlm["spin_rate"]), curr_gc3["Spin Rate"], float(curr_mlm["spin_axis"]), - curr_gc3["Spin Axis Tilt"], abs(float(curr_mlm["spin_rate"]) - curr_gc3["Spin Rate"])))
            except:
                f.write("Shot: {} MLM RPM: {} GC3 RPM: {} MLM SA: {} GC3 SA: {} RPM Error: {:.2f}\n".format(curr_mlm["source_pth"].split("USA/")[-1],\
                     float(curr_mlm["spin_rate"]), curr_gc3["Spin Rate"], float(curr_mlm["spin_axis"]), - curr_gc3["Spin Axis Tilt"], abs(float(curr_mlm["spin_rate"]) - curr_gc3["Spin Rate"])))
        except:
            try:
                f.write("Shot: {} MLM RPM: {:.2f} GC3 RPM: {:.2f} MLM SA: {:.2f} GC3 SA: {:.2f} RPM Error: {:.2f}\n".format(curr_mlm["source_pth"].split("USA/")[-1],\
                     float(curr_mlm["spin_rate"]), curr_gc3["Spin Rate"], float(curr_mlm["spin_axis"]), curr_gc3["Spin Axis Tilt"], abs(float(curr_mlm["spin_rate"]) - curr_gc3["Spin Rate"])))
            except:
                f.write("Shot: {} MLM RPM: {} GC3 RPM: {} MLM SA: {} GC3 SA: {} RPM Error: {:.2f}\n".format(curr_mlm["source_pth"].split("USA/")[-1],\
                     float(curr_mlm["spin_rate"]), curr_gc3["Spin Rate"], float(curr_mlm["spin_axis"]), curr_gc3["Spin Axis Tilt"], abs(float(curr_mlm["spin_rate"]) - curr_gc3["Spin Rate"])))
    else:
        pass
        # f.write("Shot: {} MLM RPM: {} MLM SA: {} \n".format(curr_mlm["source_pth"].split("USA/")[-1],\
        #         curr_mlm["spin_rate"], curr_mlm["spin_axis"]))

rpm_error = np.array(rpm_error)
sa_error = np.array(sa_error)
abs_rpm_error = np.array(abs_rpm_error)
abs_sa_error = np.array(abs_sa_error)

mean_rpm_error = np.mean(rpm_error)
mean_sa_error= np.mean(sa_error)
mean_abs_rpm_error = np.mean(abs_rpm_error)
mean_abs_sa_error= np.mean(abs_sa_error)

final_str = "\n\ncount1: {} count1: {} mean_rpm_error: {} mean_sa_error: {} mean_abs_rpm_error: {} mean_abs_sa_error: {}\n".format(len(sa_error),len(rpm_error), mean_rpm_error, mean_sa_error, mean_abs_rpm_error, mean_abs_sa_error)
print(final_str)
f.write(final_str)
f.close()

plt.figure()
plt.hist(abs_rpm_error, bins=[50*i for i in range(52)])
plt.title("ABS RPM ERROR HISTOGRAM")
plt.xlabel("Abs Rpm Error")
plt.xlabel("Count")
plt.grid()
plt.savefig(rpm_hist_pth)

plt.figure()
plt.hist(abs_sa_error, bins=[i for i in range(30)])
plt.title("ABS SA ERROR HISTOGRAM")
plt.xlabel("Abs SA Error")
plt.xlabel("Count")
plt.grid()
plt.savefig(sa_hist_pth)




