import csv 
import sys

# trackman_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/1_26_22/TM3_normal_1.26.Normalized.csv"
trackman_pth = ["/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/1_24_22/6ft mlm 2.Normalized.csv",
                "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/1_24_22/TM3_JB 6.5 ft MLM 2 Normal.Normalized.csv",
                "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/1_24_22/TM3_JB 7.5 ft MLM2 normal.Normalized.csv",
                "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/1_24_22/TM3_JB 8.5 ft MLM2 normal.Normalized.csv"]
mlm2_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/1_24_22/ball_numbers.txt"
mlm2_trackman_pth = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/1_24_22/ball_numbers_with_trackman.txt"

names = None
trackman_shot_list = []
mlm2_shot_list = []

match_trackman_2_mlm2 = dict()
match_mlm2_2_trackman = dict()


for t in trackman_pth:
    with open(t,encoding='cp1252') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        counter = -1
        for row in csv_reader:
            counter += 1
            print("{}\n".format(row))
            if counter == 1:
                names = row
            elif counter == 0 or counter == 2:
                continue
            else:
                data_dict = dict()
                for i in range(len(names)):
                    if names[i] == "Date":
                        curr_date = row[i].split(" ")[1]
                        curr_h = int(curr_date.split(":")[0])
                        curr_m = int(curr_date.split(":")[1])
                        curr_s = int(curr_date.split(":")[2])
                        if "PM" in row[i] and curr_h < 12:
                            curr_h += 12
                        data_dict["{}".format(names[i])] = "{}:{}:{}".format(curr_h, curr_m, curr_s)
                    else:
                        data_dict["{}".format(names[i])] = row[i]
                trackman_shot_list.append(data_dict)

with open(mlm2_pth, "r") as f:
    data = f.readlines()
    for d in data:
        curr_d = d.split(" ") 
        data_dict = dict()
        data_dict["ball_number"] = int(d.split("Img Num: ")[-1])
        data_dict["source_pth"] = d.split("Shot: ")[-1].split("Img Num: ")[0]
        parts = data_dict["source_pth"].split("/")
        riff_name = [p for p in parts if "RIFF_" in p][0]
        riff_name = riff_name.split("_")
        date = "{}:{}:{}".format(riff_name[-3], riff_name[-2], riff_name[-1])
        data_dict["Date"] = date
        mlm2_shot_list.append(data_dict)


for i in range(len(trackman_shot_list)):
    for j in range(len(mlm2_shot_list)):
        curr_trackman = trackman_shot_list[i]
        curr_mlm2 = mlm2_shot_list[j]
        date_trackman = curr_trackman["Date"]
        date_mlm2 = curr_mlm2["Date"]

        h0 = date_trackman.split(":")[0]
        h1 = date_mlm2.split(":")[0]

        m0 = date_trackman.split(":")[1]
        m1 = date_mlm2.split(":")[1]

        s0 = date_trackman.split(":")[2]
        s1 = date_mlm2.split(":")[2]

        d0 = int(h0) * 3600 + int(m0) * 60 + int(s0)
        d1 = int(h1) * 3600 + int(m1) * 60 + int(s1)

        if abs(d0 - d1) < 20:
            if not "{}".format(i) in match_trackman_2_mlm2.keys():
                match_trackman_2_mlm2["{}".format(i)] = "{}".format(j)
            else:
                sys.exit(-1)
            
            if not "{}".format(j) in match_mlm2_2_trackman.keys():
                match_mlm2_2_trackman["{}".format(j)] = "{}".format(i)
            else:
                sys.exit(-1)

f = open(mlm2_trackman_pth, "w")

for i in range(len(mlm2_shot_list)):
    curr_mlm = mlm2_shot_list[i]
    if "{}".format(i) in match_mlm2_2_trackman.keys():
        j = int(match_mlm2_2_trackman["{}".format(i)])
    else:
        j = None
    if not j is None:
        curr_trackman = trackman_shot_list[j]
        f.write("Shot: {} Img Num: {} TM Ball Speed: {} TM LA: {} TM LD: {}\n".format(curr_mlm["source_pth"].split("USA/")[-1],\
                curr_mlm["ball_number"], curr_trackman["Ball Speed"], curr_trackman["Launch Angle"], curr_trackman["Launch Direction"]))
    else:
        f.write("Shot: {} Img Num: {}\n".format(curr_mlm["source_pth"].split("USA/")[-1],\
                curr_mlm["ball_number"]))
f.close()





