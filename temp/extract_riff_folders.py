from genericpath import exists
import os
from pathlib import Path
import shutil

pth_in = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/temp_test"
pth_out = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/temp_test2"

source_PATH = Path(pth_in)
scn_folders = [str(s) for s in source_PATH.glob('**/RIFF_*/')]
RIFF_names=[s.split("/")[-1] for s in scn_folders]

for idx , p in enumerate(scn_folders):
    os.makedirs(os.path.join(pth_out, RIFF_names[idx]), exist_ok=True)
    main_pth = os.path.join(p, "gscamOuts")
    shutil.copytree(main_pth, os.path.join(pth_out, RIFF_names[idx], "gscamOuts"))