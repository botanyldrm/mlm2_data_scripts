import enum
import os
from zipfile import ZipFile
from pathlib import Path
import shutil

def unzip (path, total_count):
    for root, dirs, files in os.walk(path):
        for file in files:
            file_name = os.path.join(root, file)
            if (not file_name.endswith('.zip')):
                total_count += 1
            else:
                currentdir = file_name[:-4]
                if not os.path.exists(currentdir):
                    os.makedirs(currentdir)
                with ZipFile(file_name) as zipObj:
                    zipObj.extractall(currentdir)
                os.remove(file_name)
                total_count = unzip(currentdir, total_count)
    return total_count
def clean_working_dir(data_folder):
    source_path = Path(data_folder)
    riff_folders = [str(s) for s in source_path.glob('**/RIFF_*/')]
    rscam_videos = [str(s) for s in source_path.glob('**/shotVideo-*/')]
    shot_folder = [str(s) for s in source_path.glob('**/shot-debug-*/')]

    riff_folders=sorted(riff_folders)
    rscam_videos=sorted(rscam_videos)
    shot_folder=sorted(shot_folder)

    for idx, r in enumerate(rscam_videos):
        shutil.move(r, riff_folders[idx])
    
    for r in riff_folders:
        shutil.move(r, data_folder)
    for s in shot_folder:
        shutil.rmtree(s)




if __name__ == "__main__":
    data_folder = "/media/rapsodo/27d0bb9e-8ddd-4b9c-95a6-53c9a3600566/MLM2/data/real/USA/01_25_22/74864"
    unzip (data_folder, 0)
    clean_working_dir(data_folder)